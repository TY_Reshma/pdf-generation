
package com.te.pdfgeneration.controller;

import java.io.ByteArrayInputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.te.pdfgeneration.pdfservice.PdfServices;

@Controller
@RequestMapping("/pdf")
public class PdfGenerationController {

	@Autowired
	private PdfServices pdfServices;

	@GetMapping("/create-pdf")
	public ResponseEntity<InputStreamResource> createPdf() {
		ByteArrayInputStream byteArrayInputStream = pdfServices.createPdf();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("Content-Disposition", "inline;file=pdf-generation.pdf");
		return ResponseEntity.ok().headers(httpHeaders).contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(byteArrayInputStream));
	}
}
