package com.te.pdfgeneration.pdfservice;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PdfServices {

	Logger LOGGER = LoggerFactory.getLogger(PdfServices.class);

	@SuppressWarnings("resource")
	public ByteArrayInputStream createPdf() {

		LOGGER.info("Create PDF started  :");

		String title = "Welcome To Techno Elevate ";
		String content = "Developement Unit Of Test Yantra ";

		ByteArrayOutputStream out = new ByteArrayOutputStream();

		Document document = new Document();

		PdfWriter.getInstance(document, out);

		document.open();

		Font titlefont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 30, Color.yellow);

		Font contentfont = FontFactory.getFont(FontFactory.TIMES_BOLDITALIC, 20, Color.BLUE);

		Paragraph titlePara = new Paragraph(title, titlefont);
		Paragraph contentBody = new Paragraph(content, contentfont);

		contentBody.setAlignment(Element.ALIGN_CENTER);
		titlePara.setAlignment(Element.ALIGN_CENTER);

		document.add(titlePara);
		document.add(contentBody);

		Font paraFont = FontFactory.getFont(FontFactory.TIMES_ITALIC, 18);

		//Paragraph paragraph = new Paragraph(content);

		//document.add(paragraph);
		document.close();
		ByteArrayInputStream inputStream = new ByteArrayInputStream(out.toByteArray());
		return inputStream;

	}
}
